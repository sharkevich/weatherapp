class HiveTypeIdConstants {
  static const int cityTypeId = 0;
  static const int citiesTypeId = 1;
  static const int coordinatesTypeId = 2;
}

class HiveBoxNameConstants {
  static const String citiesBoxName = "CITIES_BOX";
  static const String currentCityBoxName = "CURRENT_CITY_BOX";
}

class HiveEntityNameConstants {
  static const String citiesEntityName = "CITIES_ENTITY_NAME";
  static const String currentCityEntityName = "CURRENT_CITY_ENTITY_NAME";
}