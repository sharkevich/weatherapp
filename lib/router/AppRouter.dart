import 'package:auto_route/auto_route.dart';
import 'package:weather/router/AppRouter.gr.dart';
import 'package:weather/router/auto_route_guard.dart';

@AutoRouterConfig()
class AppRouter extends $AppRouter {
  @override
  RouteType get defaultRouteType => const RouteType.adaptive();

  @override
  List<AutoRoute> get routes => [
        AutoRoute(
            page: HomeRoute.page,
            initial: true,
            guards: [CheckIsEmptyCities()]),
        AutoRoute(page: AddCityRoute.page)
      ];
}
