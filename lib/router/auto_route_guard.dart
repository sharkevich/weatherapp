import 'package:auto_route/auto_route.dart';
import 'package:weather/domain/repositories/CityRepository.dart';
import 'package:weather/injection.dart';
import 'package:weather/router/AppRouter.gr.dart';

class CheckIsEmptyCities extends AutoRouteGuard {
  @override
  void onNavigation(NavigationResolver resolver, StackRouter router) async {
    final CityRepository cityRepository = sl();
    await cityRepository.getCities().take(1).first.then((cities) =>
        cities.isEmpty
            ? router.push(AddCityRoute(titleText: "Add first city"))
            : resolver.next(true));
  }
}
