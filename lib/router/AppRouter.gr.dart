// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i3;
import 'package:flutter/material.dart' as _i4;
import 'package:weather/view/add_city_screen/AddCityScreen.dart' as _i1;
import 'package:weather/view/home_screen/HomeScreen.dart' as _i2;

abstract class $AppRouter extends _i3.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i3.PageFactory> pagesMap = {
    AddCityRoute.name: (routeData) {
      final args = routeData.argsAs<AddCityRouteArgs>();
      return _i3.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i1.AddCityScreen(
          key: args.key,
          titleText: args.titleText,
        ),
      );
    },
    HomeRoute.name: (routeData) {
      return _i3.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.HomeScreen(),
      );
    },
  };
}

/// generated route for
/// [_i1.AddCityScreen]
class AddCityRoute extends _i3.PageRouteInfo<AddCityRouteArgs> {
  AddCityRoute({
    _i4.Key? key,
    required String titleText,
    List<_i3.PageRouteInfo>? children,
  }) : super(
          AddCityRoute.name,
          args: AddCityRouteArgs(
            key: key,
            titleText: titleText,
          ),
          initialChildren: children,
        );

  static const String name = 'AddCityRoute';

  static const _i3.PageInfo<AddCityRouteArgs> page =
      _i3.PageInfo<AddCityRouteArgs>(name);
}

class AddCityRouteArgs {
  const AddCityRouteArgs({
    this.key,
    required this.titleText,
  });

  final _i4.Key? key;

  final String titleText;

  @override
  String toString() {
    return 'AddCityRouteArgs{key: $key, titleText: $titleText}';
  }
}

/// generated route for
/// [_i2.HomeScreen]
class HomeRoute extends _i3.PageRouteInfo<void> {
  const HomeRoute({List<_i3.PageRouteInfo>? children})
      : super(
          HomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const _i3.PageInfo<void> page = _i3.PageInfo<void>(name);
}
