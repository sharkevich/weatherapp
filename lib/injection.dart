import 'package:get_it/get_it.dart';
import 'package:weather/data/datasources/geocoding_datasource/GeocodingDatasource.dart';
import 'package:weather/data/datasources/geocoding_datasource/GeocodingDatasourceRestApiImpl.dart';
import 'package:weather/data/datasources/hive_datasource/HiveDataSourceImpl.dart';
import 'package:weather/data/datasources/hive_datasource/HiveDatasource.dart';
import 'package:weather/data/datasources/weather_datasource/WeatherDatasource.dart';
import 'package:weather/data/datasources/weather_datasource/WeatherDatasourceImpl.dart';
import 'package:weather/data/repositories/CityRepositoryImpl.dart';
import 'package:weather/data/repositories/GeocodingRepositoryImpl.dart';
import 'package:weather/data/repositories/WeatherRepositoryImpl.dart';
import 'package:weather/data/services/HiveService.dart';
import 'package:weather/data/services/OpenWeatherMapApiService.dart';
import 'package:weather/data/services/RestApiService.dart';
import 'package:weather/domain/repositories/CityRepository.dart';
import 'package:weather/domain/repositories/GeocodingRepository.dart';
import 'package:weather/domain/repositories/WeatherRepository.dart';
import 'package:weather/router/AppRouter.dart';
import 'package:weather/view/add_city_screen/add_city_bloc.dart';
import 'package:weather/view/error_controller.dart';
import 'package:weather/view/home_screen/home_screen_bloc.dart';

final sl = GetIt.instance;

class Injection {
  static Future<void> init() async {
    await _initServices();
    _initDatasource();
    _initRepositories();
    _initBlocs();
  }

  static _initServices() async {
    sl.registerSingleton<AppRouter>(AppRouter());
    sl.registerFactory<RestApiService>(() => OpenWeatherMapApiService());
    await HiveService.init();
    sl.registerSingleton<ErrorController>(ErrorController());
  }

  static _initDatasource() {
    sl.registerSingleton<GeocodingDatasource>(
        GeocodingDatasourceRestApiImpl(restApiService: sl()));
    sl.registerSingleton<HiveDatasource>(HiveDataSourceImpl());
    sl.registerSingleton<WeatherDatasource>(
        WeatherDatasourceImpl(restApiService: sl()));
  }

  static _initRepositories() {
    sl.registerSingleton<CityRepository>(
        CityRepositoryImpl(hiveDatasource: sl()));
    sl.registerSingleton<GeocodingRepository>(
        GeocodingRepositoryImpl(geocodingDatasource: sl()));
    sl.registerSingleton<WeatherRepository>(
        WeatherRepositoryImpl(weatherDatasource: sl()));
  }

  static _initBlocs() {
    sl.registerFactory<AddCityBloc>(
        () => AddCityBloc(cityRepository: sl(), geocodingRepository: sl()));
    sl.registerFactory<HomeScreenBloc>(
        () => HomeScreenBloc(cityRepository: sl(), weatherRepository: sl()));
  }
}
