class GeocodingApiResponseDataModel {
  String? name;
  LocalNames? localNames;
  double? lat;
  double? lon;
  String? country;
  String? state;

  GeocodingApiResponseDataModel(
      {this.name,
      this.localNames,
      this.lat,
      this.lon,
      this.country,
      this.state});

  GeocodingApiResponseDataModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    localNames = json['local_names'] != null
        ? LocalNames.fromJson(json['local_names'])
        : null;
    lat = json['lat'];
    lon = json['lon'];
    country = json['country'];
    state = json['state'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    if (localNames != null) {
      data['local_names'] = localNames!.toJson();
    }
    data['lat'] = lat;
    data['lon'] = lon;
    data['country'] = country;
    data['state'] = state;
    return data;
  }
}

class LocalNames {
  String? ms;
  String? gu;
  String? wa;
  String? mg;
  String? gl;
  String? om;
  String? ku;
  String? tw;
  String? mk;
  String? ee;
  String? fj;
  String? gd;
  String? ky;
  String? yo;
  String? zu;
  String? bg;
  String? tk;
  String? co;
  String? sh;
  String? de;
  String? kl;
  String? bi;
  String? km;
  String? lt;
  String? fi;
  String? fy;
  String? ba;
  String? sc;
  String? featureName;
  String? ja;
  String? am;
  String? sk;
  String? mr;
  String? es;
  String? sq;
  String? te;
  String? br;
  String? uz;
  String? da;
  String? sw;
  String? fa;
  String? sr;
  String? cu;
  String? ln;
  String? na;
  String? wo;
  String? ig;
  String? to;
  String? ta;
  String? mt;
  String? ar;
  String? su;
  String? ab;
  String? ps;
  String? bm;
  String? mi;
  String? kn;
  String? kv;
  String? os;
  String? bn;
  String? li;
  String? vi;
  String? zh;
  String? eo;
  String? ha;
  String? tt;
  String? lb;
  String? ce;
  String? hu;
  String? it;
  String? tl;
  String? pl;
  String? sm;
  String? en;
  String? vo;
  String? el;
  String? sn;
  String? fr;
  String? cs;
  String? io;
  String? hi;
  String? et;
  String? pa;
  String? av;
  String? ko;
  String? bh;
  String? yi;
  String? sa;
  String? sl;
  String? hr;
  String? si;
  String? so;
  String? gn;
  String? ay;
  String? se;
  String? sd;
  String? af;
  String? ga;
  String? or;
  String? ia;
  String? ie;
  String? ug;
  String? nl;
  String? gv;
  String? qu;
  String? be;
  String? an;
  String? fo;
  String? hy;
  String? nv;
  String? bo;
  String? ascii;
  String? id;
  String? lv;
  String? ca;
  String? no;
  String? nn;
  String? ml;
  String? my;
  String? ne;
  String? he;
  String? cy;
  String? lo;
  String? jv;
  String? sv;
  String? mn;
  String? tg;
  String? kw;
  String? cv;
  String? az;
  String? oc;
  String? th;
  String? ru;
  String? ny;
  String? bs;
  String? st;
  String? ro;
  String? rm;
  String? ff;
  String? kk;
  String? uk;
  String? pt;
  String? tr;
  String? eu;
  String? ht;
  String? ka;
  String? ur;

  LocalNames(
      {this.ms,
      this.gu,
      this.wa,
      this.mg,
      this.gl,
      this.om,
      this.ku,
      this.tw,
      this.mk,
      this.ee,
      this.fj,
      this.gd,
      this.ky,
      this.yo,
      this.zu,
      this.bg,
      this.tk,
      this.co,
      this.sh,
      this.de,
      this.kl,
      this.bi,
      this.km,
      this.lt,
      this.fi,
      this.fy,
      this.ba,
      this.sc,
      this.featureName,
      this.ja,
      this.am,
      this.sk,
      this.mr,
      this.es,
      this.sq,
      this.te,
      this.br,
      this.uz,
      this.da,
      this.sw,
      this.fa,
      this.sr,
      this.cu,
      this.ln,
      this.na,
      this.wo,
      this.ig,
      this.to,
      this.ta,
      this.mt,
      this.ar,
      this.su,
      this.ab,
      this.ps,
      this.bm,
      this.mi,
      this.kn,
      this.kv,
      this.os,
      this.bn,
      this.li,
      this.vi,
      this.zh,
      this.eo,
      this.ha,
      this.tt,
      this.lb,
      this.ce,
      this.hu,
      this.it,
      this.tl,
      this.pl,
      this.sm,
      this.en,
      this.vo,
      this.el,
      this.sn,
      this.fr,
      this.cs,
      this.io,
      this.hi,
      this.et,
      this.pa,
      this.av,
      this.ko,
      this.bh,
      this.yi,
      this.sa,
      this.sl,
      this.hr,
      this.si,
      this.so,
      this.gn,
      this.ay,
      this.se,
      this.sd,
      this.af,
      this.ga,
      this.or,
      this.ia,
      this.ie,
      this.ug,
      this.nl,
      this.gv,
      this.qu,
      this.be,
      this.an,
      this.fo,
      this.hy,
      this.nv,
      this.bo,
      this.ascii,
      this.id,
      this.lv,
      this.ca,
      this.no,
      this.nn,
      this.ml,
      this.my,
      this.ne,
      this.he,
      this.cy,
      this.lo,
      this.jv,
      this.sv,
      this.mn,
      this.tg,
      this.kw,
      this.cv,
      this.az,
      this.oc,
      this.th,
      this.ru,
      this.ny,
      this.bs,
      this.st,
      this.ro,
      this.rm,
      this.ff,
      this.kk,
      this.uk,
      this.pt,
      this.tr,
      this.eu,
      this.ht,
      this.ka,
      this.ur});

  LocalNames.fromJson(Map<String, dynamic> json) {
    ms = json['ms'];
    gu = json['gu'];
    wa = json['wa'];
    mg = json['mg'];
    gl = json['gl'];
    om = json['om'];
    ku = json['ku'];
    tw = json['tw'];
    mk = json['mk'];
    ee = json['ee'];
    fj = json['fj'];
    gd = json['gd'];
    ky = json['ky'];
    yo = json['yo'];
    zu = json['zu'];
    bg = json['bg'];
    tk = json['tk'];
    co = json['co'];
    sh = json['sh'];
    de = json['de'];
    kl = json['kl'];
    bi = json['bi'];
    km = json['km'];
    lt = json['lt'];
    fi = json['fi'];
    fy = json['fy'];
    ba = json['ba'];
    sc = json['sc'];
    featureName = json['feature_name'];
    ja = json['ja'];
    am = json['am'];
    sk = json['sk'];
    mr = json['mr'];
    es = json['es'];
    sq = json['sq'];
    te = json['te'];
    br = json['br'];
    uz = json['uz'];
    da = json['da'];
    sw = json['sw'];
    fa = json['fa'];
    sr = json['sr'];
    cu = json['cu'];
    ln = json['ln'];
    na = json['na'];
    wo = json['wo'];
    ig = json['ig'];
    to = json['to'];
    ta = json['ta'];
    mt = json['mt'];
    ar = json['ar'];
    su = json['su'];
    ab = json['ab'];
    ps = json['ps'];
    bm = json['bm'];
    mi = json['mi'];
    kn = json['kn'];
    kv = json['kv'];
    os = json['os'];
    bn = json['bn'];
    li = json['li'];
    vi = json['vi'];
    zh = json['zh'];
    eo = json['eo'];
    ha = json['ha'];
    tt = json['tt'];
    lb = json['lb'];
    ce = json['ce'];
    hu = json['hu'];
    it = json['it'];
    tl = json['tl'];
    pl = json['pl'];
    sm = json['sm'];
    en = json['en'];
    vo = json['vo'];
    el = json['el'];
    sn = json['sn'];
    fr = json['fr'];
    cs = json['cs'];
    io = json['io'];
    hi = json['hi'];
    et = json['et'];
    pa = json['pa'];
    av = json['av'];
    ko = json['ko'];
    bh = json['bh'];
    yi = json['yi'];
    sa = json['sa'];
    sl = json['sl'];
    hr = json['hr'];
    si = json['si'];
    so = json['so'];
    gn = json['gn'];
    ay = json['ay'];
    se = json['se'];
    sd = json['sd'];
    af = json['af'];
    ga = json['ga'];
    or = json['or'];
    ia = json['ia'];
    ie = json['ie'];
    ug = json['ug'];
    nl = json['nl'];
    gv = json['gv'];
    qu = json['qu'];
    be = json['be'];
    an = json['an'];
    fo = json['fo'];
    hy = json['hy'];
    nv = json['nv'];
    bo = json['bo'];
    ascii = json['ascii'];
    id = json['id'];
    lv = json['lv'];
    ca = json['ca'];
    no = json['no'];
    nn = json['nn'];
    ml = json['ml'];
    my = json['my'];
    ne = json['ne'];
    he = json['he'];
    cy = json['cy'];
    lo = json['lo'];
    jv = json['jv'];
    sv = json['sv'];
    mn = json['mn'];
    tg = json['tg'];
    kw = json['kw'];
    cv = json['cv'];
    az = json['az'];
    oc = json['oc'];
    th = json['th'];
    ru = json['ru'];
    ny = json['ny'];
    bs = json['bs'];
    st = json['st'];
    ro = json['ro'];
    rm = json['rm'];
    ff = json['ff'];
    kk = json['kk'];
    uk = json['uk'];
    pt = json['pt'];
    tr = json['tr'];
    eu = json['eu'];
    ht = json['ht'];
    ka = json['ka'];
    ur = json['ur'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ms'] = ms;
    data['gu'] = gu;
    data['wa'] = wa;
    data['mg'] = mg;
    data['gl'] = gl;
    data['om'] = om;
    data['ku'] = ku;
    data['tw'] = tw;
    data['mk'] = mk;
    data['ee'] = ee;
    data['fj'] = fj;
    data['gd'] = gd;
    data['ky'] = ky;
    data['yo'] = yo;
    data['zu'] = zu;
    data['bg'] = bg;
    data['tk'] = tk;
    data['co'] = co;
    data['sh'] = sh;
    data['de'] = de;
    data['kl'] = kl;
    data['bi'] = bi;
    data['km'] = km;
    data['lt'] = lt;
    data['fi'] = fi;
    data['fy'] = fy;
    data['ba'] = ba;
    data['sc'] = sc;
    data['feature_name'] = featureName;
    data['ja'] = ja;
    data['am'] = am;
    data['sk'] = sk;
    data['mr'] = mr;
    data['es'] = es;
    data['sq'] = sq;
    data['te'] = te;
    data['br'] = br;
    data['uz'] = uz;
    data['da'] = da;
    data['sw'] = sw;
    data['fa'] = fa;
    data['sr'] = sr;
    data['cu'] = cu;
    data['ln'] = ln;
    data['na'] = na;
    data['wo'] = wo;
    data['ig'] = ig;
    data['to'] = to;
    data['ta'] = ta;
    data['mt'] = mt;
    data['ar'] = ar;
    data['su'] = su;
    data['ab'] = ab;
    data['ps'] = ps;
    data['bm'] = bm;
    data['mi'] = mi;
    data['kn'] = kn;
    data['kv'] = kv;
    data['os'] = os;
    data['bn'] = bn;
    data['li'] = li;
    data['vi'] = vi;
    data['zh'] = zh;
    data['eo'] = eo;
    data['ha'] = ha;
    data['tt'] = tt;
    data['lb'] = lb;
    data['ce'] = ce;
    data['hu'] = hu;
    data['it'] = it;
    data['tl'] = tl;
    data['pl'] = pl;
    data['sm'] = sm;
    data['en'] = en;
    data['vo'] = vo;
    data['el'] = el;
    data['sn'] = sn;
    data['fr'] = fr;
    data['cs'] = cs;
    data['io'] = io;
    data['hi'] = hi;
    data['et'] = et;
    data['pa'] = pa;
    data['av'] = av;
    data['ko'] = ko;
    data['bh'] = bh;
    data['yi'] = yi;
    data['sa'] = sa;
    data['sl'] = sl;
    data['hr'] = hr;
    data['si'] = si;
    data['so'] = so;
    data['gn'] = gn;
    data['ay'] = ay;
    data['se'] = se;
    data['sd'] = sd;
    data['af'] = af;
    data['ga'] = ga;
    data['or'] = or;
    data['ia'] = ia;
    data['ie'] = ie;
    data['ug'] = ug;
    data['nl'] = nl;
    data['gv'] = gv;
    data['qu'] = qu;
    data['be'] = be;
    data['an'] = an;
    data['fo'] = fo;
    data['hy'] = hy;
    data['nv'] = nv;
    data['bo'] = bo;
    data['ascii'] = ascii;
    data['id'] = id;
    data['lv'] = lv;
    data['ca'] = ca;
    data['no'] = no;
    data['nn'] = nn;
    data['ml'] = ml;
    data['my'] = my;
    data['ne'] = ne;
    data['he'] = he;
    data['cy'] = cy;
    data['lo'] = lo;
    data['jv'] = jv;
    data['sv'] = sv;
    data['mn'] = mn;
    data['tg'] = tg;
    data['kw'] = kw;
    data['cv'] = cv;
    data['az'] = az;
    data['oc'] = oc;
    data['th'] = th;
    data['ru'] = ru;
    data['ny'] = ny;
    data['bs'] = bs;
    data['st'] = st;
    data['ro'] = ro;
    data['rm'] = rm;
    data['ff'] = ff;
    data['kk'] = kk;
    data['uk'] = uk;
    data['pt'] = pt;
    data['tr'] = tr;
    data['eu'] = eu;
    data['ht'] = ht;
    data['ka'] = ka;
    data['ur'] = ur;
    return data;
  }
}
