import 'package:weather/data/models/weather/weather_api_response_data_model.dart';
import 'package:weather/data/models/weather/weather_data/city_api_data_model.dart';

class ForecastFiveDaysApiResponseDataModel {
  String? cod;
  int? message;
  int? cnt;
  List<WeatherApiResponsDataModel>? list;
  CityApiDataModel? city;

  ForecastFiveDaysApiResponseDataModel(
      {this.cod, this.message, this.cnt, this.list, this.city});

  ForecastFiveDaysApiResponseDataModel.fromJson(Map<String, dynamic> json) {
    cod = json['cod'];
    message = json['message'];
    cnt = json['cnt'];
    if (json['list'] != null) {
      list = <WeatherApiResponsDataModel>[];
      json['list'].forEach((v) {
        list!.add(WeatherApiResponsDataModel.fromJson(v));
      });
    }
    city = json['city'] != null ? CityApiDataModel.fromJson(json['city']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['cod'] = cod;
    data['message'] = message;
    data['cnt'] = cnt;
    if (list != null) {
      data['list'] = list!.map((v) => v.toJson()).toList();
    }
    if (city != null) {
      data['city'] = city!.toJson();
    }
    return data;
  }
}
