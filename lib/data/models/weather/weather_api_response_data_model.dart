import 'package:weather/data/models/weather/weather_data/clouds_api_data_model.dart';
import 'package:weather/data/models/weather/weather_data/coord_api_data_model.dart';
import 'package:weather/data/models/weather/weather_data/main_api_data_model.dart';
import 'package:weather/data/models/weather/weather_data/rain_api_data_model.dart';
import 'package:weather/data/models/weather/weather_data/sys_api_data_model.dart';
import 'package:weather/data/models/weather/weather_data/weather_data_model.dart';
import 'package:weather/data/models/weather/weather_data/wind_api_data_model.dart';

class WeatherApiResponsDataModel {
  CoordApiDataModel? coord;
  List<WeatherApiDataModel>? weather;
  String? base;
  MainApiDataModel? main;
  int? visibility;
  WindApiDataModel? wind;
  RainApiDataModel? rain;
  CloudsAoiDataModel? clouds;
  int? dt;
  SysApiDataModel? sys;
  int? timezone;
  int? id;
  String? name;
  int? cod;
  double? pop;
  String? dtTxt;

  WeatherApiResponsDataModel(
      {this.coord,
      this.weather,
      this.base,
      this.main,
      this.visibility,
      this.wind,
      this.rain,
      this.clouds,
      this.dt,
      this.sys,
      this.timezone,
      this.id,
      this.name,
      this.cod,
      this.pop,
      this.dtTxt});

  WeatherApiResponsDataModel.fromJson(Map<String, dynamic> json) {
    coord = json['coord'] != null
        ? CoordApiDataModel.fromJson(json['coord'])
        : null;
    if (json['weather'] != null) {
      weather = <WeatherApiDataModel>[];
      json['weather'].forEach((v) {
        weather!.add(WeatherApiDataModel.fromJson(v));
      });
    }
    base = json['base'];
    main =
        json['main'] != null ? MainApiDataModel.fromJson(json['main']) : null;
    visibility = json['visibility'];
    wind =
        json['wind'] != null ? WindApiDataModel.fromJson(json['wind']) : null;
    rain =
        json['rain'] != null ? RainApiDataModel.fromJson(json['rain']) : null;
    clouds = json['clouds'] != null
        ? CloudsAoiDataModel.fromJson(json['clouds'])
        : null;
    dt = json['dt'];
    sys = json['sys'] != null ? SysApiDataModel.fromJson(json['sys']) : null;
    timezone = json['timezone'];
    id = json['id'];
    name = json['name'];
    cod = json['cod'];
    pop = json['pop']?.toDouble();
    dtTxt = json['dtTxt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (coord != null) {
      data['coord'] = coord!.toJson();
    }
    if (weather != null) {
      data['weather'] = weather!.map((v) => v.toJson()).toList();
    }
    data['base'] = base;
    if (main != null) {
      data['main'] = main?.toJson();
    }
    data['visibility'] = visibility;
    if (wind != null) {
      data['wind'] = wind!.toJson();
    }
    if (rain != null) {
      data['rain'] = rain!.toJson();
    }
    if (clouds != null) {
      data['clouds'] = clouds!.toJson();
    }
    data['dt'] = dt;
    if (sys != null) {
      data['sys'] = sys!.toJson();
    }
    data['timezone'] = timezone;
    data['id'] = id;
    data['name'] = name;
    data['cod'] = cod;
    data['pop'] = pop;
    data['dtTxt'] = dtTxt;
    return data;
  }
}
