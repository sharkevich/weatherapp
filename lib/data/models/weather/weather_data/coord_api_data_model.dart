class CoordApiDataModel {
  double? lon;
  double? lat;

  CoordApiDataModel({this.lon, this.lat});

  CoordApiDataModel.fromJson(Map<String, dynamic> json) {
    lon = json['lon']?.toDouble();
    lat = json['lat']?.toDouble();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['lon'] = lon;
    data['lat'] = lat;
    return data;
  }
}
