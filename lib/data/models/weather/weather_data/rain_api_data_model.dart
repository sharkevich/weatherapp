class RainApiDataModel {
  double? d1h;

  RainApiDataModel({this.d1h});

  RainApiDataModel.fromJson(Map<String, dynamic> json) {
    d1h = json['1h'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['1h'] = d1h;
    return data;
  }
}
