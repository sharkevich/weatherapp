class WindApiDataModel {
  double? speed;
  int? deg;
  double? gust;

  WindApiDataModel({this.speed, this.deg, this.gust});

  WindApiDataModel.fromJson(Map<String, dynamic> json) {
    speed = json['speed']?.toDouble();
    deg = json['deg'];
    gust = json['gust']?.toDouble();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['speed'] = speed;
    data['deg'] = deg;
    data['gust'] = gust;
    return data;
  }
}
