import 'package:hive_flutter/hive_flutter.dart';
import 'package:weather/domain/entities/Cities.dart';
import 'package:weather/domain/entities/City.dart';
import 'package:weather/domain/entities/Coordinates.dart';

class HiveService {
  static Future<void> init() async {
    await Hive.initFlutter();
    Hive.registerAdapter(CityAdapter());
    Hive.registerAdapter(CitiesAdapter());
    Hive.registerAdapter(CoordinatesAdapter());
  }
}