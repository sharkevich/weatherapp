import 'package:weather/data/services/Service.dart';

abstract class RestApiService implements Service {
  String _authority;
  String _unencodedPath;
  RequestMethod _requestMethod;
  Map<String, String>? _header;
  Map<String, dynamic>? _body;
  Map<String, dynamic>? _queryParameters;

  RestApiService()
      : _authority = "",
        _unencodedPath = "",
        _requestMethod = RequestMethod.get;

  String get authority => _authority;

  Map<String, dynamic>? get queryParameters => _queryParameters;

  String get unencodedPath => _unencodedPath;

  RequestMethod get requestMethod => _requestMethod;

  Map<String, String>? get header => _header;

  Map<String, dynamic>? get body => _body;

  set queryParameters(Map<String, dynamic>? value) {
    _queryParameters = value;
  }

  addQueryParameters(Map<String, dynamic> value) {
    if (_queryParameters != null) {
      _queryParameters!.addAll(value);
    } else {
      _queryParameters = value;
    }
  }

  set body(Map<String, dynamic>? value) {
    _body = value;
  }

  set header(Map<String, String>? value) {
    _header = value;
  }

  set requestMethod(RequestMethod value) {
    _requestMethod = value;
  }

  set unencodedPath(String value) {
    _unencodedPath = value;
  }

  set authority(String value) {
    _authority = value;
  }
}

enum RequestMethod {
  get("GET"),
  delete("DELETE"),
  post("POST");

  final String method;

  const RequestMethod(this.method);
}
