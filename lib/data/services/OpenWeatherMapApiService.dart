import 'dart:convert';

import 'package:http/http.dart';
import 'package:weather/data/services/RestApiService.dart';

class OpenWeatherMapApiService extends RestApiService {
  final String apiKey = "e097f5db775c1049bf60c4a9f7e0b3c0";

  OpenWeatherMapApiService() {
    authority = "api.openweathermap.org";
    queryParameters = {"appid": apiKey};
  }

  @override
  Future<String> send() async {
    final url = Uri.https(authority, unencodedPath, queryParameters);
    final request = Request(requestMethod.method, url);
    request.body = jsonEncode(body);
    if (header != null) {
      request.headers.addAll(header!);
    }
    final response = await Client().send(request);
    if (response.statusCode == 200) {
      return await response.stream.bytesToString(const Utf8Codec());
    } else {
      final stringBody = await response.stream.bytesToString(const Utf8Codec());
      throw Exception(stringBody);
    }
  }
}
