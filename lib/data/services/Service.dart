abstract class Service {

  Future<dynamic> send();
}