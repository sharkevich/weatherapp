import 'package:weather/data/models/weather/forecast_five_days_api_response_data_model.dart';
import 'package:weather/data/models/weather/weather_api_response_data_model.dart';
import 'package:weather/domain/entities/City.dart';
import 'package:weather/domain/entities/Weather.dart';

extension WeatherMapper on WeatherApiResponsDataModel {
  Weather toWeather({City? city}) {
    return Weather(
        city: city,
        cloudsAll: clouds?.all,
        date: _convertDate(dt ?? 0),
        descriptionWeather: weather?.first.description,
        feelsLike: _kelvinToCelsius(main?.feelsLike),
        humidity: main?.humidity,
        mainWeather: weather?.first.main,
        rain: rain?.d1h,
        temperature: _kelvinToCelsius(main?.temp),
        temperatureMax: _kelvinToCelsius(main?.tempMax),
        temperatureMin: _kelvinToCelsius(main?.tempMin),
        visibility: visibility,
        windDeg: _windDegToString(wind?.deg ?? 0),
        windSpeed: wind?.speed,
        iconUrl: _getIconUrl(weather?.first.icon));
  }

  String? _getIconUrl(String? iconId) {
    return iconId != null
        ? "https://openweathermap.org/img/wn/$iconId@2x.png"
        : null;
  }

  DateTime _convertDate(int dt) {
    return DateTime.fromMillisecondsSinceEpoch(dt * 1000, isUtc: true).toLocal();
  }

  _kelvinToCelsius(double? temp) {
    return temp == null ? null : temp - 273.15;
  }

  _windDegToString(int deg) {
    if (deg > 23 && deg < 68) {
      return "N/E";
    } else if (deg >= 68 && deg < 113) {
      return "E";
    } else if (deg >= 113 && deg < 158) {
      return "S/E";
    } else if (deg >= 158 && deg < 205) {
      return "S";
    } else if (deg >= 205 && deg < 249) {
      return "S/W";
    } else if (deg >= 249 && deg < 294) {
      return "W";
    } else if (deg >= 294 && deg < 338) {
      return "N/W";
    } else if (deg >= 338 && deg <= 365) {
      return "N";
    } else if (deg >= 0 && deg < 23) {
      return "N";
    }
  }
}

extension WeatherMapperFromForecast on ForecastFiveDaysApiResponseDataModel {
  List<Weather> toWeather({City? city}) {
    return list?.map((e) => e.toWeather(city: city)).toList() ?? [];
  }
}
