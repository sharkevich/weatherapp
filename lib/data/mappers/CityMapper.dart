import 'package:weather/data/models/geocoding/geocoding_api_data_model.dart';
import 'package:weather/domain/entities/City.dart';
import 'package:weather/domain/entities/Coordinates.dart';

extension CityMapper on GeocodingApiResponseDataModel {
  City toCity() {
    return City(name: name, coord: Coordinates(lat: lat, lon: lon));
  }
}
