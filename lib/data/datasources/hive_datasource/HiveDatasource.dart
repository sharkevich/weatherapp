abstract class HiveDatasource {
  Future<T?> getData<T>({required String nameBox, required String name});

  Future saveData<T>(
      {required String nameBox, required T data, required String name});
}
