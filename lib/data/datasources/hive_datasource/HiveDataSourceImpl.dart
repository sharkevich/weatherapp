import 'package:hive_flutter/hive_flutter.dart';
import 'package:weather/data/datasources/hive_datasource/HiveDatasource.dart';

class HiveDataSourceImpl extends HiveDatasource {
  HiveDataSourceImpl();

  Future<Box> _getBox<T>(String nameBox) async {
    return Hive.isBoxOpen(nameBox)
        ? Hive.box<T>(nameBox)
        : await Hive.openBox<T>(nameBox);
  }

  @override
  Future<T?> getData<T>({required String nameBox, required String name}) async {
    final data = (await _getBox<T>(nameBox)).get(name);
    return data;
  }

  @override
  Future saveData<T>(
      {required String nameBox, required T data, required String name}) async {
    await (await _getBox<T>(nameBox)).put(name, data);
  }
}
