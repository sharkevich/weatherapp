import 'dart:convert';

import 'package:weather/data/datasources/geocoding_datasource/GeocodingDatasource.dart';
import 'package:weather/data/mappers/CityMapper.dart';
import 'package:weather/data/models/geocoding/geocoding_api_data_model.dart';
import 'package:weather/data/services/RestApiService.dart';
import 'package:weather/domain/entities/City.dart';

class GeocodingDatasourceRestApiImpl extends GeocodingDatasource {

  final RestApiService restApiService;

  GeocodingDatasourceRestApiImpl({required this.restApiService});

  @override
  Future<List<City>> getCitiesFromString({required String text}) async {
    restApiService.unencodedPath = "geo/1.0/direct";
    restApiService.addQueryParameters({
      "q": text
    });
    restApiService.requestMethod = RequestMethod.get;
    var request = await restApiService.send();
    var data = jsonDecode(request);
    List<City> model = data.map<City>((element) => GeocodingApiResponseDataModel.fromJson(element).toCity()).toList();
    return model;
  }
}