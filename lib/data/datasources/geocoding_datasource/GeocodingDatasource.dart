import 'package:weather/domain/entities/City.dart';

abstract class GeocodingDatasource {

  Future<List<City>> getCitiesFromString({required String text});
}