import 'package:weather/domain/entities/City.dart';
import 'package:weather/domain/entities/Weather.dart';

abstract class WeatherDatasource {

  Future<Weather> getWeather({required City city});

  Future<List<Weather>> getForecast({required City city});
}