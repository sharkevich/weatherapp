import 'dart:convert';

import 'package:weather/data/datasources/weather_datasource/WeatherDatasource.dart';
import 'package:weather/data/mappers/WeatherMapper.dart';
import 'package:weather/data/models/weather/forecast_five_days_api_response_data_model.dart';
import 'package:weather/data/models/weather/weather_api_response_data_model.dart';
import 'package:weather/data/services/RestApiService.dart';
import 'package:weather/domain/entities/City.dart';
import 'package:weather/domain/entities/Weather.dart';

class WeatherDatasourceImpl extends WeatherDatasource {
  final RestApiService restApiService;

  WeatherDatasourceImpl({required this.restApiService});

  @override
  Future<Weather> getWeather({required City city}) async {
    restApiService.unencodedPath = "data/2.5/weather";
    restApiService.addQueryParameters({
      "lat": city.coord?.lat.toString(),
      "lon": city.coord?.lon.toString()});
    restApiService.requestMethod = RequestMethod.get;
    var request = await restApiService.send();
    Weather model = WeatherApiResponsDataModel.fromJson(jsonDecode(request))
        .toWeather(city: city);
    return model;
  }

  @override
  Future<List<Weather>> getForecast({required City city}) async {
    restApiService.unencodedPath = "data/2.5/forecast";
    restApiService.addQueryParameters({
      "lat": city.coord?.lat.toString(),
      "lon": city.coord?.lon.toString()});
    restApiService.requestMethod = RequestMethod.get;
    var request = await restApiService.send();
    List<Weather> model = ForecastFiveDaysApiResponseDataModel.fromJson(jsonDecode(request))
        .toWeather(city: city);
    return model;
  }
}
