import 'package:weather/data/datasources/weather_datasource/WeatherDatasource.dart';
import 'package:weather/domain/entities/City.dart';
import 'package:weather/domain/entities/Weather.dart';
import 'package:weather/domain/repositories/WeatherRepository.dart';

class WeatherRepositoryImpl extends WeatherRepository {
  final WeatherDatasource weatherDatasource;

  WeatherRepositoryImpl({required this.weatherDatasource});

  @override
  Stream<Weather> getWeather({required City city}) async* {
    yield await weatherDatasource.getWeather(city: city);
  }

  @override
  Stream<List<Weather>> getForecast({required City city}) async* {
    yield await weatherDatasource.getForecast(city: city);
  }
}
