import 'package:weather/core/constants.dart';
import 'package:weather/data/datasources/hive_datasource/HiveDatasource.dart';
import 'package:weather/domain/entities/Cities.dart';
import 'package:weather/domain/entities/City.dart';
import 'package:weather/domain/repositories/CityRepository.dart';

class CityRepositoryImpl extends CityRepository {
  final HiveDatasource hiveDatasource;
  final String citiesBoxName = HiveBoxNameConstants.citiesBoxName;
  final String citiesEntityName = HiveEntityNameConstants.citiesEntityName;
  final String currentCityBoxName = HiveBoxNameConstants.currentCityBoxName;
  final String currentCityEntityName =
      HiveEntityNameConstants.currentCityEntityName;

  CityRepositoryImpl({required this.hiveDatasource});

  Future<List<City>> _getCities() async {
    var data = (await hiveDatasource.getData<Cities>(
                nameBox: citiesBoxName, name: citiesEntityName))
            ?.cities ??
        [];
    return data;
  }

  Future _setCities(List<City> cities) async {
    return await hiveDatasource.saveData(
        nameBox: citiesBoxName,
        data: Cities(cities: cities),
        name: citiesEntityName);
  }

  @override
  Stream<List<City>> getCities() async* {
    yield await _getCities();
  }

  @override
  Stream<bool> saveCities({required City city}) async* {
    List<City> cities = await _getCities();
    cities.add(city);
    try {
      await _setCities(cities);
      yield true;
    } catch (_) {
      throw Exception();
    }
  }

  @override
  Stream<bool> deleteCity({required City city}) async* {
    List<City> cities = await _getCities();
    cities.remove(city);
    try {
      await _setCities(cities);
      yield true;
    } catch (_) {
      throw Exception();
    }
  }

  @override
  Stream<City?> getCurrentCity() async* {
    yield await hiveDatasource.getData<City>(
        nameBox: currentCityBoxName, name: currentCityEntityName);
  }

  @override
  Stream<bool> saveCurrentCity({required City city}) async* {
    try {
      await await hiveDatasource.saveData(
          nameBox: currentCityBoxName, data: city, name: currentCityEntityName);
      yield true;
    } catch (_) {
      throw Exception();
    }
  }
}
