import 'package:weather/data/datasources/geocoding_datasource/GeocodingDatasource.dart';
import 'package:weather/domain/entities/City.dart';
import 'package:weather/domain/repositories/GeocodingRepository.dart';

class GeocodingRepositoryImpl extends GeocodingRepository {

  final GeocodingDatasource geocodingDatasource;

  GeocodingRepositoryImpl({required this.geocodingDatasource});

  @override
  Stream<List<City>> getCitiesFromString({required String text}) async* {
    yield await geocodingDatasource.getCitiesFromString(text: text);
  }

}