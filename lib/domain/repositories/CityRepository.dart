import 'package:weather/domain/entities/City.dart';

abstract class CityRepository {

  Stream<List<City>> getCities();
  Stream<bool> saveCities({required City city});
  Stream<bool> deleteCity({required City city});
  Stream<City?> getCurrentCity();
  Stream<bool> saveCurrentCity({required City city});
}