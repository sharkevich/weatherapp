import 'package:weather/domain/entities/City.dart';

abstract class GeocodingRepository {

  Stream<List<City>> getCitiesFromString({required String text});
}