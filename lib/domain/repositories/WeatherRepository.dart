import 'package:weather/domain/entities/City.dart';
import 'package:weather/domain/entities/Weather.dart';

abstract class WeatherRepository {

  Stream<Weather> getWeather({required City city});

  Stream<List<Weather>> getForecast({required City city});
}