import 'package:hive_flutter/hive_flutter.dart';
import 'package:weather/core/constants.dart';
import 'package:weather/domain/entities/City.dart';

part 'Cities.g.dart';

@HiveType(typeId: HiveTypeIdConstants.citiesTypeId)
class Cities {
  @HiveField(0)
  final List<City> cities;

  Cities({required this.cities});
}