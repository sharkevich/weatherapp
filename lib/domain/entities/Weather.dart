import 'package:weather/domain/entities/City.dart';

class Weather {
  final City? city;
  final String? mainWeather;
  final String? descriptionWeather;
  final double? temperature;
  final double? feelsLike;
  final double? temperatureMax;
  final double? temperatureMin;
  final int? humidity;
  final int? visibility;
  final double? windSpeed;
  final String? windDeg;
  final int? cloudsAll;
  final double? rain;
  final DateTime? date;
  final String? iconUrl;

  Weather(
      {this.city,
      this.mainWeather,
      this.descriptionWeather,
      this.temperature,
      this.feelsLike,
      this.temperatureMax,
      this.temperatureMin,
      this.humidity,
      this.visibility,
      this.windSpeed,
      this.windDeg,
      this.cloudsAll,
      this.rain,
      this.date,
      this.iconUrl});
}
