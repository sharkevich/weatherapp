
import 'package:hive_flutter/hive_flutter.dart';
import 'package:weather/core/constants.dart';

part 'Coordinates.g.dart';

@HiveType(typeId: HiveTypeIdConstants.coordinatesTypeId)
class Coordinates {
  @HiveField(0)
  final double? lat;
  @HiveField(1)
  final double? lon;

  Coordinates({this.lat, this.lon});
}