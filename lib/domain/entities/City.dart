import 'package:hive_flutter/hive_flutter.dart';
import 'package:weather/core/constants.dart';
import 'package:weather/domain/entities/Coordinates.dart';

part 'City.g.dart';
@HiveType(typeId: HiveTypeIdConstants.cityTypeId)
class City {
  @HiveField(0)
  final String? name;
  @HiveField(1)
  final Coordinates? coord;

  City({this.name, this.coord});
}