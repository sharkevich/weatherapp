// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Cities.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CitiesAdapter extends TypeAdapter<Cities> {
  @override
  final int typeId = 1;

  @override
  Cities read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Cities(
      cities: (fields[0] as List).cast<City>(),
    );
  }

  @override
  void write(BinaryWriter writer, Cities obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.cities);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CitiesAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
