import 'package:flutter/foundation.dart';
import 'package:weather/injection.dart';
import 'package:weather/view/error_controller.dart';

class BaseBloc {
  void sendError(String errorText) {
    if (kDebugMode) {
      print(errorText);
    }
    sl<ErrorController>().errorStreamController.add(errorText);
  }
}
