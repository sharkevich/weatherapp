import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:weather/domain/entities/City.dart';
import 'package:weather/injection.dart';
import 'package:weather/view/add_city_screen/add_city_bloc.dart';
import 'package:weather/view/error_controller.dart';
import 'package:weather/view/widgets/Toast.dart';

@RoutePage()
class AddCityScreen extends StatefulWidget {
  final String titleText;

  const AddCityScreen({super.key, required this.titleText});

  @override
  State<AddCityScreen> createState() => _AddCityScreenState();
}

class _AddCityScreenState extends State<AddCityScreen> {
  final TextEditingController textEditingController = TextEditingController();
  final AddCityBloc bloc = sl();

  @override
  void initState() {
    sl<ErrorController>().errorStreamController.stream.listen((error) {
      Toast.showToast(message: error);
    });
    textEditingController.addListener(() {
      bloc.searchCity(text: textEditingController.text);
    });
    super.initState();
  }

  Widget cityItem(String text, Function() callback) {
    final borderRadius = BorderRadius.circular(5);
    return ClipRRect(
        borderRadius: borderRadius,
        child: InkWell(
            onTap: callback,
            child: Container(
                width: double.infinity,
                height: 40,
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.12),
                    borderRadius: borderRadius),
                alignment: Alignment.centerLeft,
                child:
                    Text(text, style: Theme.of(context).textTheme.bodyLarge))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(widget.titleText)),
        body: SafeArea(
            child: Padding(
                padding: const EdgeInsets.only(
                    top: 24, left: 16, right: 16, bottom: 32),
                child: Column(children: [
                  TextFormField(
                      controller: textEditingController,
                      decoration: const InputDecoration(
                          prefixIcon: Icon(Icons.search),
                          hintText: "city name",
                          border: OutlineInputBorder())),
                  const SizedBox(height: 24),
                  StreamBuilder(
                      stream: bloc.foundCitiesStreamController.stream,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return ListView.builder(
                              shrinkWrap: true,
                              itemCount: snapshot.data?.length,
                              itemBuilder: (BuildContext context, int index) {
                                City city = snapshot.data![index];
                                return cityItem(
                                    city.name ?? "", () => bloc.saveCity(city));
                              });
                        } else {
                          return Container();
                        }
                      })
                ]))));
  }
}
