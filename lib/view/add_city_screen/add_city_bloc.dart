import 'dart:async';

import 'package:weather/domain/entities/City.dart';
import 'package:weather/domain/repositories/CityRepository.dart';
import 'package:weather/domain/repositories/GeocodingRepository.dart';
import 'package:weather/injection.dart';
import 'package:weather/router/AppRouter.dart';
import 'package:weather/router/AppRouter.gr.dart';
import 'package:weather/view/base_bloc.dart';

class AddCityBloc extends BaseBloc {
  final GeocodingRepository geocodingRepository;
  final CityRepository cityRepository;
  final StreamController<List<City>> foundCitiesStreamController =
      StreamController();

  AddCityBloc(
      {required this.geocodingRepository, required this.cityRepository});

  saveCity(City city) {
    if (city.name != null &&
        city.coord?.lon != null &&
        city.coord?.lat != null) {
      _addCityToLocalStorage(city);
    } else {
      sendError("City select error");
    }
  }

  searchCity({required String text}) {
    if (text.length > 1) {
      _geocoding(text);
    } else {
      _sendCities([]);
    }
  }

  void _sendCities(List<City> cities) {
    foundCitiesStreamController.add(cities);
  }

  void _geocoding(String text) {
    geocodingRepository.getCitiesFromString(text: text).listen((cities) {
      if (cities.isNotEmpty) {
        _sendCities(cities);
      } else {
        _sendCities([]);
        sendError("City is not found");
      }
    }, onError: (_) => sendError("Error geocoding"));
  }

  void _addCityToLocalStorage(City city) {
    cityRepository.saveCities(city: city).listen((event) {
      _saveCurrentCity(city);
    },
        onError: (_) =>
            sendError("Error select city! Error Add City To Local Storage"));
  }

  void _saveCurrentCity(City city) {
    cityRepository.saveCurrentCity(city: city).listen((event) {
      sl<AppRouter>()
          .replaceAll([const HomeRoute()], updateExistingRoutes: false);
    },
        onError: (_) =>
            sendError("Error select city! Error Add City To Local Storage"));
  }
}
