import 'dart:async';

import 'package:weather/domain/entities/City.dart';
import 'package:weather/domain/entities/Weather.dart';
import 'package:weather/domain/repositories/CityRepository.dart';
import 'package:weather/domain/repositories/WeatherRepository.dart';
import 'package:weather/injection.dart';
import 'package:weather/router/AppRouter.dart';
import 'package:weather/router/AppRouter.gr.dart';
import 'package:weather/view/base_bloc.dart';

class HomeScreenBloc extends BaseBloc {
  final CityRepository cityRepository;
  final WeatherRepository weatherRepository;
  final StreamController<Weather> weatherStreamController = StreamController();
  final StreamController<List<Weather>> forecastStreamController =
      StreamController();
  final StreamController<List<City>> citiesStreamController =
      StreamController();

  List<City> cities = [];
  City currentCity = City();

  HomeScreenBloc(
      {required this.cityRepository, required this.weatherRepository});

  updateData() {
    _getCurrentCity().then((currentCity) {
      this.currentCity = currentCity;
      _getCities().then((cities) {
        this.cities = cities;
        citiesStreamController.add(cities);
        _fetchWeather(currentCity);
        _fetchForecast(currentCity);
      }, onError: (_) => sendError("Error get Cities"));
    }, onError: (_) => sendError("Current city not found"));
  }

  deleteCity(City city) {
    _deleteCity(city).then((value) {
      cities.remove(city);
      if (cities.isEmpty) {
        sl<AppRouter>().replaceAll([AddCityRoute(titleText: "Ann first city")],
            updateExistingRoutes: false);
      } else {
        if (city == currentCity) {
          _saveCurrentCity(cities.first).then((value) => updateData(),
              onError: (_) => sendError("Error save city"));
        } else {
          updateData();
        }
      }
    }, onError: (_) => sendError("Error delete city"));
  }

  switchCity(City city) {
    _saveCurrentCity(city).then((value) {
      updateData();
    }, onError: (_) => sendError("Error save city"));
  }

  void _fetchWeather(City city) {
    weatherRepository.getWeather(city: city).listen((weather) {
      weatherStreamController.add(weather);
    }, onError: (_) => sendError("Error update weather"));
  }

  void _fetchForecast(City city) {
    weatherRepository.getForecast(city: city).listen((event) {
      forecastStreamController.add(event);
    }, onError: (_) => sendError("Error update forecast"));
  }

  Future _getCities() => cityRepository.getCities().first;

  Future _getCurrentCity() => cityRepository.getCurrentCity().first;

  Future _deleteCity(City city) => cityRepository.deleteCity(city: city).first;

  Future _saveCurrentCity(City city) =>
      cityRepository.saveCurrentCity(city: city).first;

  dispose() {
    weatherStreamController.close();
    forecastStreamController.close();
    citiesStreamController.close();
  }
}
