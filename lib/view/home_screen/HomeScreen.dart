import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:weather/domain/entities/City.dart';
import 'package:weather/domain/entities/Weather.dart';
import 'package:weather/injection.dart';
import 'package:weather/view/error_controller.dart';
import 'package:weather/view/home_screen/home_screen_bloc.dart';
import 'package:weather/view/widgets/CitiesNavigationDrawer.dart';
import 'package:weather/view/widgets/ForecastWidget.dart';
import 'package:weather/view/widgets/InfoDetailsWeatherCardWidget.dart';
import 'package:weather/view/widgets/TemperatureWidget.dart';
import 'package:weather/view/widgets/Toast.dart';
import 'package:weather/view/widgets/WeatherDetailsWidget.dart';

@RoutePage()
class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final HomeScreenBloc bloc = sl();
  List<City> cities = [];

  @override
  void initState() {
    sl<ErrorController>().errorStreamController.stream.listen((error) {
      Toast.showToast(message: error);
    });
    bloc.citiesStreamController.stream.listen((event) {
      if (cities != event) {
        setState(() {
          cities = event;
        });
      }
    });
    super.initState();
  }

  Widget body() {
    Widget temperatureWidgetAppBar(Weather weather) => TemperatureWidgetAppBar(
        key: UniqueKey(),
        cityName: weather.city?.name ?? "",
        descriptionWeather: weather.descriptionWeather,
        feelsLike: weather.feelsLike,
        mainWeather: weather.mainWeather,
        temperature: weather.temperature,
        iconUrl: weather.iconUrl,
        action: OutlinedButton(
            style: OutlinedButton.styleFrom(
                side: BorderSide(color: Theme.of(context).colorScheme.primary)),
            onPressed: () => scaffoldKey.currentState!.openEndDrawer(),
            child: const Text("City")));

    Widget weatherDetailsWidget(Weather weather) => SliverToBoxAdapter(
        child: Padding(
            padding: const EdgeInsets.only(top: 24.0),
            child: WeatherDetailsWidget(
                key: UniqueKey(),
                cloudsAll: weather.cloudsAll,
                rain: weather.rain,
                temperatureMax: weather.temperatureMax,
                temperatureMin: weather.temperatureMin,
                visibility: weather.visibility)));

    Widget infoRowWidgets(Weather weather) => SliverToBoxAdapter(
        child: Padding(
            padding: const EdgeInsets.only(top: 24.0),
            child: Row(children: [
              Expanded(
                  child: InfoDetailsWeatherCardWidget(
                      key: UniqueKey(),
                      image: 'assets/ic_wind.svg',
                      description: '${weather.windDeg}',
                      title: '${weather.windSpeed.toString()} km/h')),
              const SizedBox(width: 24),
              Expanded(
                  child: InfoDetailsWeatherCardWidget(
                      key: UniqueKey(),
                      image: 'assets/ic_humidity.svg',
                      description: '${weather.humidity}%',
                      title: 'Humidity'))
            ])));

    final forecastWidget = SliverToBoxAdapter(
        child: StreamBuilder(
            stream: bloc.forecastStreamController.stream,
            builder: (context, snapshot) {
              if (snapshot.data?.isNotEmpty ?? false) {
                return Padding(
                    padding: const EdgeInsets.only(top: 24.0),
                    child: ForecastWidget(
                        key: UniqueKey(), forecast: snapshot.data!));
              } else {
                return Container();
              }
            }));

    Widget lastUpdateWidget(Weather weather) => SliverToBoxAdapter(
        child: Padding(
            padding: const EdgeInsets.only(top: 24, left: 8, right: 8),
            child: Text(
                key: UniqueKey(),
                "Weather updated: ${DateFormat("dd.MM.yyyy HH:mm").format(weather.date ?? DateTime.now())}",
                style: Theme.of(context)
                    .textTheme
                    .titleMedium
                    ?.copyWith(color: Colors.black.withOpacity(0.7)))));

    return StreamBuilder(
        stream: bloc.weatherStreamController.stream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final weather = snapshot.data!;
            return CustomScrollView(slivers: [
              temperatureWidgetAppBar(weather),
              weatherDetailsWidget(weather),
              infoRowWidgets(weather),
              forecastWidget,
              lastUpdateWidget(weather)
            ]);
          } else {
            bloc.updateData();
            return const Center(child: CircularProgressIndicator());
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        body: SafeArea(
            child: Padding(
          padding:
              const EdgeInsets.only(top: 24, left: 18, right: 18, bottom: 32),
          child: body(),
        )),
        endDrawer: CitiesNavigationDrawer(
            onTap: (city) {
              bloc.switchCity(city);
              scaffoldKey.currentState!.closeEndDrawer();
            },
            delete: (city) {
              bloc.deleteCity(city);
              scaffoldKey.currentState!.closeEndDrawer();
            },
            cities: cities));
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }
}
