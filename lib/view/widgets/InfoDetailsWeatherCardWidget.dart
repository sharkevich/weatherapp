import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class InfoDetailsWeatherCardWidget extends StatelessWidget {
  final String image;
  final String title;
  final String description;

  const InfoDetailsWeatherCardWidget(
      {super.key,
      required this.image,
      required this.title,
      required this.description});

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset(image, height: 50),
                  const SizedBox(height: 24),
                  Text(title,
                      style: Theme.of(context)
                          .textTheme
                          .titleLarge
                          ?.copyWith(fontWeight: FontWeight.w700)),
                  const SizedBox(height: 12),
                  Text(description,
                      style: Theme.of(context).textTheme.titleLarge)
                ])));
  }
}
