import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:weather/view/widgets/widget_handler.dart';

class TemperatureWidgetAppBar extends StatelessWidget {
  final String cityName;
  final String? mainWeather;
  final String? descriptionWeather;
  final double? temperature;
  final double? feelsLike;
  final Widget? action;
  final String? iconUrl;

  const TemperatureWidgetAppBar(
      {super.key,
      required this.cityName,
      this.mainWeather,
      this.descriptionWeather,
      this.temperature,
      this.feelsLike,
      this.action,
      this.iconUrl});

  @override
  Widget build(BuildContext context) {
    return SliverPersistentHeader(
        delegate: _TemperatureWidgetDelegate(
            cityName: cityName,
            temperature: temperature ?? 0,
            mainWeather: mainWeather,
            feelsLike: feelsLike,
            action: action,
            iconUrl: iconUrl,
            descriptionWeather: descriptionWeather),
        pinned: true);
  }
}

class _TemperatureWidgetDelegate extends SliverPersistentHeaderDelegate {
  final String cityName;
  final String? mainWeather;
  final String? descriptionWeather;
  final double temperature;
  final double? feelsLike;
  final Widget? action;
  final String? iconUrl;

  _TemperatureWidgetDelegate(
      {required this.cityName,
      this.mainWeather,
      this.descriptionWeather,
      required this.temperature,
      this.action,
      this.iconUrl,
      this.feelsLike});

  String get tempString => "${NumberFormat("#").format(temperature)}°";

  String get tempFeelsLikeString =>
      "${NumberFormat("#.#").format(feelsLike)}°C";

  double opacity(double shrinkOffset) => shrinkOffset == 0
      ? 1
      : shrinkOffset < minVisibilityOffsetBodyWidget
          ? (minVisibilityOffsetBodyWidget - shrinkOffset) /
              minVisibilityOffsetBodyWidget
          : 0;

  double headerWidgetOpacity(double shrinkOffset) =>
      shrinkOffset < maxExtent / 1.5 ? shrinkOffset * 1.5 / maxExtent : 1;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    const padding = EdgeInsets.all(10);

    Widget? mainWeatherWidget =
        WidgetHandler.getMainWeatherWidget(iconUrl, mainWeather);

    Widget header(double shrinkOffset) {
      return Container(
          height: minExtent - (padding.top + padding.bottom),
          alignment: Alignment.centerLeft,
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Opacity(
                opacity: headerWidgetOpacity(shrinkOffset),
                child: Row(children: [
                  Text("$cityName $tempString, ",
                      style: Theme.of(context).textTheme.titleLarge),
                  if (mainWeatherWidget != null) mainWeatherWidget
                ])),
            action ?? Container()
          ]));
    }

    Widget weatherWidget() {
      return Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(tempString,
                style: Theme.of(context).textTheme.displayLarge?.copyWith(
                    fontSize: 100,
                    color: Theme.of(context).colorScheme.primary)),
            const SizedBox(width: 24),
            Column(children: [
              if (mainWeather != null) mainWeatherWidget!,
              if (descriptionWeather != null)
                Text(descriptionWeather!,
                    style: Theme.of(context).textTheme.titleLarge?.copyWith(
                        color: Theme.of(context).colorScheme.primary))
            ])
          ]);
    }

    Widget weatherDescriptionWidget() {
      return Expanded(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
            Text(cityName, style: Theme.of(context).textTheme.displayLarge),
            if (feelsLike != null)
              Text("Feels like $tempFeelsLikeString",
                  style: Theme.of(context).textTheme.titleLarge)
          ]));
    }

    Widget body(double shrinkOffset) {
      return FittedBox(
          fit: BoxFit.fitWidth,
          child: SizedBox(
              height: maxExtent - minExtent,
              width: MediaQuery.of(context).size.width,
              child: Opacity(
                  opacity: opacity(shrinkOffset),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        weatherWidget(),
                        weatherDescriptionWidget()
                      ]))));
    }

    return Container(
        padding: padding,
        color: Theme.of(context).colorScheme.background,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              header(shrinkOffset),
              if (shrinkOffset < minVisibilityOffsetBodyWidget)
                Expanded(child: body(shrinkOffset))
            ]));
  }

  @override
  double get maxExtent => 300;

  @override
  double get minExtent => 80;

  double get minVisibilityOffsetBodyWidget => 90;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) =>
      true;
}
