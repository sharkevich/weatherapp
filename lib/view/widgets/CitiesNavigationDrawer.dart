import 'package:flutter/material.dart';
import 'package:weather/domain/entities/City.dart';
import 'package:weather/injection.dart';
import 'package:weather/router/AppRouter.dart';
import 'package:weather/router/AppRouter.gr.dart';

class CitiesNavigationDrawer extends StatelessWidget {
  final List<City> cities;
  final Function(City city) onTap;
  final Function(City city) delete;

  const CitiesNavigationDrawer(
      {super.key,
      required this.cities,
      required this.onTap,
      required this.delete});

  @override
  Widget build(BuildContext context) {
    Widget titleWidget =
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Text("Select City", style: Theme.of(context).textTheme.titleLarge),
      IconButton(
          onPressed: () =>
              sl<AppRouter>().push(AddCityRoute(titleText: "Add new city")),
          icon: Icon(Icons.add_circle_outline,
              size: 35, color: Theme.of(context).colorScheme.primary))
    ]);

    Widget buttonsWidget = ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: cities.length,
        itemBuilder: (context, index) {
          return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(children: [
                Expanded(
                    child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                            side: BorderSide(
                                color: Theme.of(context).colorScheme.primary)),
                        onPressed: () => onTap(cities[index]),
                        child: Text(cities[index].name ?? ""))),
                const SizedBox(width: 24),
                IconButton(
                    onPressed: () => delete(cities[index]),
                    icon: const Icon(Icons.delete))
              ]));
        });

    return NavigationDrawer(children: [
      Container(
          padding: const EdgeInsets.all(10),
          child: Column(children: [
            titleWidget,
            const SizedBox(height: 24),
            buttonsWidget
          ]))
    ]);
  }
}
