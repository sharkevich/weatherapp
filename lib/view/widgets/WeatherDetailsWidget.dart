import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class WeatherDetailsWidget extends StatelessWidget {
  final double? temperatureMax;
  final double? temperatureMin;
  final int? visibility;
  final int? cloudsAll;
  final double? rain;

  const WeatherDetailsWidget(
      {super.key,
      this.temperatureMax,
      this.temperatureMin,
      this.visibility,
      this.cloudsAll,
      this.rain});

  Widget item(String label, String data, BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 12),
        child: Row(children: [
          Text("$label: ", style: Theme.of(context).textTheme.titleLarge),
          Text(data,
              style: Theme.of(context)
                  .textTheme
                  .titleLarge
                  ?.copyWith(fontWeight: FontWeight.w500, color: Theme.of(context).colorScheme.primary))
        ]));
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Padding(
            padding: const EdgeInsets.all(10),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              if (temperatureMax != null)
                item("Temperature today, max",
                    "${NumberFormat("#").format(temperatureMax)}°", context),
              if (temperatureMin != null)
                item("Temperature today, min",
                    "${NumberFormat("#").format(temperatureMin)}°", context),
              if (visibility != null)
                item("Visibility", "$visibility m", context),
              if (cloudsAll != null) item("Clouds", "$cloudsAll%", context),
              if (rain != null)
                item("Rain for the last hour", "$rain mm", context)
            ])));
  }
}
