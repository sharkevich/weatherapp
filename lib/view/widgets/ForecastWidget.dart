import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:weather/domain/entities/Weather.dart';
import 'package:weather/view/widgets/widget_handler.dart';

class ForecastWidget extends StatelessWidget {
  final List<Weather> forecast;

  const ForecastWidget({super.key, required this.forecast});

  Widget forecastItemWidget(Weather weather, BuildContext context) {
    Widget withBottomPadding(double bottom, Widget child) =>
        Padding(padding: EdgeInsets.only(bottom: bottom), child: child);

    Widget? mainWeatherWidget() =>
        WidgetHandler.getMainWeatherWidget(weather.iconUrl, weather.mainWeather,
            style: Theme.of(context).textTheme.titleLarge);

    return Container(
        width: 150,
        margin: const EdgeInsets.all(8),
        padding: const EdgeInsets.all(8),
        child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
          if (mainWeatherWidget() != null)
            withBottomPadding(
                10,
                Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Theme.of(context).colorScheme.background),
                    child: mainWeatherWidget()!)),
          withBottomPadding(
              8,
              Text("${NumberFormat("#").format(weather.temperature)}°",
                  style: Theme.of(context).textTheme.displaySmall?.copyWith(
                      color: Theme.of(context).colorScheme.primary))),
          withBottomPadding(
              8,
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                SvgPicture.asset('assets/ic_humidity.svg', height: 25),
                const SizedBox(width: 12),
                Text("${weather.humidity.toString()}%",
                    style: Theme.of(context).textTheme.titleLarge)
              ])),
          Container(
              padding: const EdgeInsets.all(10),
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Theme.of(context).colorScheme.background),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      DateFormat('HH:mm')
                          .format(weather.date ?? DateTime.now()),
                      style: Theme.of(context).textTheme.titleLarge?.copyWith(
                          color: Theme.of(context).colorScheme.primary),
                    ),
                    Text(
                        DateFormat('dd.MM')
                            .format(weather.date ?? DateTime.now()),
                        style: Theme.of(context).textTheme.titleLarge)
                  ]))
        ]));
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        child: SizedBox(
            height: 326,
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: ListView.builder(
                    itemCount: forecast.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      return forecastItemWidget(forecast[index], context);
                    }))));
  }
}
