import 'package:flutter/material.dart';

class WidgetHandler {
  static Widget? getMainWeatherWidget(String? iconUrl, String? mainWeather,
      {TextStyle? style}) {
    if (iconUrl == null) {
      if (mainWeather == null) {
        return null;
      } else {
        return Text(mainWeather, style: style);
      }
    } else {
      return Image.network(iconUrl, fit: BoxFit.fill);
    }
  }
}
